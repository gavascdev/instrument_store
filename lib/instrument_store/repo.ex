defmodule InstrumentStore.Repo do
  use Ecto.Repo,
    otp_app: :instrument_store,
    adapter: Ecto.Adapters.Postgres
end
