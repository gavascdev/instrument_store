defmodule InstrumentStore.Instruments do
  @moduledoc """
  The Instruments context.
  """

  import Ecto.Query, warn: false
  alias InstrumentStore.Repo

  alias InstrumentStore.Instruments.Guitar

  def list_guitars do
    Repo.all(Guitar)
  end

  def get_guitar!(id), do: Repo.get!(Guitar, id)

  def create_guitar(attrs \\ %{}) do
    %Guitar{}
    |> Guitar.changeset(attrs)
    |> Repo.insert()
  end

  def delete_guitar(%Guitar{} = guitar) do
    Repo.delete(guitar)
  end

  def change_guitar(%Guitar{} = guitar, attrs \\ %{}) do
    Guitar.changeset(guitar, attrs)
  end
end
